package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task startByID(String id) {
        final Task task = findByID(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        return null;
    }

    @Override
    public Task startByName(String name) {
        return null;
    }

    @Override
    public Task finishByID(String id) {
        return null;
    }

    @Override
    public Task finishByIndex(Integer index) {
        return null;
    }

    @Override
    public Task finishByName(String name) {
        return null;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task removeByID(final String id) {
        final Task task = findByID(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findByID(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task changeStatusByID(String id, Status status) {
        final Task task = findByID(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(final String id) {
        return findByID(id) != null;
    }

    @Override
    public Task bindTaskToProjectById(String projectId, String taskId) {
        final Task task = findByID(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        List<Task> taskProject = new ArrayList<Task>();
        for (Task task : tasks) {
            String id = task.getProjectId();
            if (projectId.equals(id)) {
                taskProject.add(task);
            }
        }
        return taskProject;
    }

    @Override
    public Task unbindTaskToProjectById(String projectId, String taskId) {
        final Task task = findByID(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(projectId);
        for (Task task : listByProject) {
            tasks.remove(task);
        }
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasksList = new ArrayList<>(tasks);
        tasksList.sort(comparator);
        return tasksList;
    }

    @Override
    public Integer getSize() {
        return tasks.size();
    }

}
