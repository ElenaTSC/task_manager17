package ru.tsk.ilina.tm.api.entity;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);
}
