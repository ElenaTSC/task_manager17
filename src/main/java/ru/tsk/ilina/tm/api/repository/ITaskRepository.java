package ru.tsk.ilina.tm.api.repository;


import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskToProjectById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task startByID(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishByID(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task removeByID(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task findByID(String id);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task changeStatusByID(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    boolean existsById(String id);

    Integer getSize();

}
