package ru.tsk.ilina.tm.exception.system;

import ru.tsk.ilina.tm.constant.TerminalConst;
import ru.tsk.ilina.tm.exception.AbstractException;

public class UnknowCommandException extends AbstractException {

    public UnknowCommandException() {
        super("Incorrect command. Use ''" + TerminalConst.HELP + "'' for display command");
    }

}
