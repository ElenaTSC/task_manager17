package ru.tsk.ilina.tm.comparator;

import ru.tsk.ilina.tm.api.entity.IHasCreated;
import ru.tsk.ilina.tm.api.entity.IHasName;

import java.util.Comparator;

public class ComparatorByName implements Comparator<IHasName> {

    private static final ComparatorByName INSTANCE = new ComparatorByName();

    public ComparatorByName() {

    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasName o1, IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getName() == null || o2.getName() == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
