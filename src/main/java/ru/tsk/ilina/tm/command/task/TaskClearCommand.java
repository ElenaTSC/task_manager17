package ru.tsk.ilina.tm.command.task;

import ru.tsk.ilina.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Delete all tasks";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

}
