package ru.tsk.ilina.tm.command.task;

import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class TaskShowByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-show-by-name";
    }

    @Override
    public String description() {
        return "Show task by name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
