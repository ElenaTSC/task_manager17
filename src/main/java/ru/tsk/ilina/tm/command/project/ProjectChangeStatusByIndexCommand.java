package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project_change_status_by_index";
    }

    @Override
    public String description() {
        return "Change status by project index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = serviceLocator.getProjectService().changeStatusByIndex(index, status);
        if (project == null) throw new ProjectNotFoundException();
    }

}
