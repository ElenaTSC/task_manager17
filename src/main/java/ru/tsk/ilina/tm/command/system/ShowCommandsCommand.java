package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;

public class ShowCommandsCommand extends AbstractCommand {

    @Override
    public String name() {
        return "commands";
    }

    @Override
    public String description() {
        return "Display list commands.";
    }

    @Override
    public String arg() {
        return "-cmd";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        for (final String command : serviceLocator.getCommandService().getListCommandName()) {
            System.out.println(command);
        }
    }

}
