package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class ProjectTaskRemoveAllByIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return "project_task_remove_all_by_id";
    }

    @Override
    public String description() {
        return "Remove all task bind project id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(projectId);
    }

}
