package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class ProjectTaskBindByIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return "project_task_bind_by_id";
    }

    @Override
    public String description() {
        return "Bind task by project id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByID(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByID(taskId);
        if (task == null) throw new TaskNotFoundException();
        final Task taskUpdate = serviceLocator.getProjectTaskService().bindTaskById(projectId, taskId);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

}
