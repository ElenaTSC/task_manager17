package ru.tsk.ilina.tm.command.task;

import ru.tsk.ilina.tm.command.AbstractTaskCommand;
import ru.tsk.ilina.tm.enumerated.Sort;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListShowCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll();
        else {
            Sort sortValue = Sort.valueOf(sort);
            System.out.println(sortValue.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(sortValue.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

}
