package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public void execute() {
        System.out.println("1.0.0");
    }

}
