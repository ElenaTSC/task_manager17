package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectTaskCommand;
import ru.tsk.ilina.tm.exception.entity.TaskNotFoundException;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskFindByIdCommand extends AbstractProjectTaskCommand {

    @Override
    public String name() {
        return "project_task_find_by_id";
    }

    @Override
    public String description() {
        return "Find task by project id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(id);
        if (tasks == null) throw new TaskNotFoundException();
        for (Task task : tasks) System.out.println(task.toString());
    }

}
