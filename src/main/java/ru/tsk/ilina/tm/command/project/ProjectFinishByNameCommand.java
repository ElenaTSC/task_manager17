package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish project by name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

}
