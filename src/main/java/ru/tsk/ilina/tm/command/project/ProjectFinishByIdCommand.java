package ru.tsk.ilina.tm.command.project;

import ru.tsk.ilina.tm.command.AbstractProjectCommand;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "Finish project by id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishByID(id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
